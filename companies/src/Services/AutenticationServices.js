import axios from "axios";
import BaseUrl from "../Config/BaseUrl";

const create = (baseURL = BaseUrl.autentication) => {
  const api = axios.create({
    baseURL,
    timeout: 10000,
    headers: {
      "Content-Type": "application/json",
    },
  });

  const signInAsync = (body) => api.post("/sign_in", body);

  return {
    signInAsync,
  };
};

export default {
  create,
};
