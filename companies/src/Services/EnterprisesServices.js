import axios from "axios";
import BaseUrl from "../Config/BaseUrl";
import Interceptors from "./Utils/Interceptors";

const create = (baseURL = BaseUrl.enterprises) => {
  const api = axios.create({
    baseURL,
    timeout: 10000,
    headers: {
      "Content-Type": "application/json",
    },
  });

  Interceptors(api);

  const getAllAsync = () => api.get("/");

  const getEnterpriseAsync = (id) => api.get(`/${id}`);

  const getEnterpriseByTypeAndNameAsync = ({ type, name }) => {
    let params = "?";
    params += !!type ? `enterprise_types=${type}&` : "";
    params += !!name ? `name=${name}` : "";
    return api.get(params);
  };

  return {
    getAllAsync,
    getEnterpriseAsync,
    getEnterpriseByTypeAndNameAsync,
  };
};

export default {
  create,
};
