const LocalStorageService = (function () {
  var _service;
  function _getService() {
    if (!_service) {
      _service = this;
      return _service;
    }
    return _service;
  }

  function _setToken(headers) {
    localStorage.setItem("access-token", headers["access-token"]);
    localStorage.setItem("client", headers["client"]);
    localStorage.setItem("token_type", headers["token-type"]);
    localStorage.setItem("uid", headers["uid"]);
  }

  function _getAccessToken() {
    return localStorage.getItem("access-token");
  }

  function _getClient() {
    return localStorage.getItem("client");
  }

  function _getUid() {
    return localStorage.getItem("uid");
  }

  function _getTokenType() {
    return localStorage.getItem("token_type");
  }

  function _clearToken() {
    localStorage.removeItem("access-token");
    localStorage.removeItem("client");
    localStorage.removeItem("uid");
    localStorage.removeItem("token_type");
  }

  return {
    getService: _getService,
    setToken: _setToken,
    getAccessToken: _getAccessToken,
    getClient: _getClient,
    getUid: _getUid,
    getTokenType: _getTokenType,
    clearToken: _clearToken,
  };
})();

export default LocalStorageService;
