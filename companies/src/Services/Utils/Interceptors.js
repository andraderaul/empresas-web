import LocalStorageService from "./LocalStorageServices";

const Interceptors = (api) => {
  const localStorageService = LocalStorageService.getService();

  // Add a request interceptor
  api.interceptors.request.use(
    async (config) => {
      const accessToken = localStorageService.getAccessToken();
      const client = localStorageService.getClient();
      const uid = localStorageService.getUid();

      config.headers["access-token"] = `${accessToken}`;
      config.headers["client"] = `${client}`;
      config.headers["uid"] = `${uid}`;

      return config;
    },
    (error) => {
      Promise.reject(error);
    }
  );

  //Add a response interceptor
  api.interceptors.response.use(
    (response) => {
      return response;
    },
    function (error) {
      if (
        error.response &&
        error.response.status &&
        error.response.status === 401
      ) {
        console.log(error.response);
        window.history.pushState({}, "signin", "/");
        window.location.reload();
        return Promise.reject(error);
      }
      return Promise.reject(error);
    }
  );
};

export default Interceptors;
