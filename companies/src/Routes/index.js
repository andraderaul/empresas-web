import React from "react";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";

import { connect } from "react-redux";

import { SignInPage, HomePage, DetailPage } from "../Pages";

const PrivateConfig = ({ user, children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        !!user.id ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};
const mapStateToProps = (state) => ({
  user: state.signIn.data.investor,
});

const PrivateRoute = connect(mapStateToProps)(PrivateConfig);

const Routes = (props) => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={SignInPage} />
        <PrivateRoute>
          <Route path="/home" component={HomePage} />
          <Route path="/detail" component={DetailPage} />
        </PrivateRoute>
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
