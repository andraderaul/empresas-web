import React from "react";
import { Provider } from "react-redux";
import "./Config/ReactotronConfig";

import "./App.css";

import Routes from "./Routes";
import store from "./Store";

function App() {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
}

export default App;
