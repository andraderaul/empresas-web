export const initialNameEnterprise = (fullName = "") => {
  return fullName
    .trim()
    .toUpperCase()
    .replace("Á", "A")
    .replace(/[^a-zA-Z0-9 ]/g, "")
    .split(" ")
    .map((name) => name[0])
    .join("");
};
