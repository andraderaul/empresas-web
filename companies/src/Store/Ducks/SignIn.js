/* Actions Types */
export const Types = {
  SIGNIN_REQUEST: "signin/SIGNIN_REQUEST",
  SIGNIN_SUCCESS: "signin/SIGNIN_SUCCESS",
  SIGNIN_FAILURE: "signin/SIGNIN_FAILURE",
};

/* Reducer */
const INITIAL_STATE = {
  isLoading: false,
  data: {
    investor: { id: "" },
  },
  error: null,
};

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.SIGNIN_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Types.SIGNIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        data: action.payload.data,
      };
    case Types.SIGNIN_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
}

/* Actions */
export const Creators = {
  signInRequest: ({ email, password }) => ({
    type: Types.SIGNIN_REQUEST,
    payload: { email, password },
  }),
  signInSuccess: (data) => ({
    type: Types.SIGNIN_SUCCESS,
    payload: { data },
  }),
  signInFailure: (error) => ({
    type: Types.SIGNIN_FAILURE,
    payload: { error },
  }),
};
