/* Actions Types */

export const Types = {
  ENTERPRISES_REQUEST: "enterprises/ENTERPRISES_REQUEST",
  ENTERPRISES_SUCCESS: "enterprises/ENTERPRISES_SUCCESS",
  ENTERPRISES_FAILURE: "enterprises/ENTERPRISES_FAILURE",
};

/* Reducer */
const INITIAL_STATE = {
  isLoading: false,
  data: { enterprises: [] },
  enterpriseTypes: [],
  error: null,
};

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.ENTERPRISES_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case Types.ENTERPRISES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        data: action.payload.data,
        enterpriseTypes:
          state.enterpriseTypes.length === 0
            ? action.payload.data.enterprises
                .map((enterprise) => enterprise.enterprise_type)
                .filter(
                  (enterprise, index, array) =>
                    array.findIndex(
                      (currentEnterprise) =>
                        currentEnterprise.id === enterprise.id
                    ) === index
                )
            : state.enterpriseTypes,
      };
    case Types.ENTERPRISES_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error,
      };

    default:
      return state;
  }
}

/* Actions */
export const Creators = {
  enterprisesRequest: ({ type, name }) => ({
    type: Types.ENTERPRISES_REQUEST,
    payload: { type, name },
  }),
  enterprisesSuccess: (data) => ({
    type: Types.ENTERPRISES_SUCCESS,
    payload: { data },
  }),
  enterprisesFailure: (error) => ({
    type: Types.ENTERPRISES_FAILURE,
    payload: { error },
  }),
};
