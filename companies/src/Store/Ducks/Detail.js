/* Actions Types */

export const Types = {
  DETAIL_REQUEST: "enterprises/DETAIL_REQUEST",
  DETAIL_SUCCESS: "enterprises/DETAIL_SUCCESS",
  DETAIL_FAILURE: "enterprises/DETAIL_FAILURE",
};

/* Reducer */
const INITIAL_STATE = {
  isLoading: false,
  data: { enterprise: {} },
  error: null,
};

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.DETAIL_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case Types.DETAIL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        data: action.payload.data,
      };
    case Types.DETAIL_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error,
      };

    default:
      return state;
  }
}

/* Actions */
export const Creators = {
  detailRequest: ({ id }) => ({
    type: Types.DETAIL_REQUEST,
    payload: { id },
  }),
  detailSuccess: (data) => ({
    type: Types.DETAIL_SUCCESS,
    payload: { data },
  }),
  detailFailure: (error) => ({
    type: Types.DETAIL_FAILURE,
    payload: { error },
  }),
};
