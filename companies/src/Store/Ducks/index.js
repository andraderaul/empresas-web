// Imports: Dependencies
import { combineReducers } from "redux";

// Imports: Reducers
import signIn from "./SignIn";
import enterprises from "./Enterprise";
import detail from "./Detail";

// Redux: Root Reducer
export default combineReducers({
  signIn,
  enterprises,
  detail,
});
