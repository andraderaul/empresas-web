import { all, takeLatest } from "redux-saga/effects";

import { Types as SignInTypes } from "../Ducks/SignIn";
import { Types as EterpriseTypes } from "../Ducks/Enterprise";
import { Types as DetailTypes } from "../Ducks/Detail";
import { signIn } from "./SignIn";
import { enterprise } from "./Enterprise";
import { detail } from "./Detail";

export default function* rootSaga() {
  yield all([
    takeLatest(SignInTypes.SIGNIN_REQUEST, signIn),
    takeLatest(EterpriseTypes.ENTERPRISES_REQUEST, enterprise),
    takeLatest(DetailTypes.DETAIL_REQUEST, detail),
  ]);
}
