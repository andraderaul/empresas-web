import { call, put } from "redux-saga/effects";

import EnterprisesServices from "../../Services/EnterprisesServices";

import { Creators as DetailActions } from "../Ducks/Detail";

export function* detail(action) {
  const enterprisesServices = EnterprisesServices.create();
  try {
    const { data } = yield call(
      enterprisesServices.getEnterpriseAsync,
      action.payload.id
    );

    yield put(DetailActions.detailSuccess(data));
  } catch (err) {
    console.log(err);
    let msgError = "";

    if (!err.response) {
      msgError = "Tempo limite excedido.";
    } else if (err.response.status === 401) {
      const {
        response: {
          data: { errors },
        },
      } = err;

      msgError =
        errors.length > 0
          ? "Você precisa estar logado para continuar"
          : "Erro Interno no Servidor";
    } else {
      msgError = "Erro Interno no Servidor.";
    }

    yield put(DetailActions.detailFailure(msgError));
  }
}
