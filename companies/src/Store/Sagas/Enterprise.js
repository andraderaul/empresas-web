import { call, put } from "redux-saga/effects";

import EnterprisesServices from "../../Services/EnterprisesServices";

import { Creators as EnterpriseActions } from "../Ducks/Enterprise";

export function* enterprise(action) {
  const enterprisesServices = EnterprisesServices.create();
  try {
    const {
      payload: { type, name },
    } = action;

    const { data } =
      !type && !name
        ? yield call(enterprisesServices.getAllAsync)
        : yield call(enterprisesServices.getEnterpriseByTypeAndNameAsync, {
            type,
            name,
          });

    yield put(EnterpriseActions.enterprisesSuccess(data));
  } catch (err) {
    console.log(err);
    let msgError = "";

    if (!err.response) {
      msgError = "Tempo limite excedido.";
    } else if (err.response.status === 401) {
      const {
        response: {
          data: { errors },
        },
      } = err;

      msgError =
        errors.length > 0
          ? "Você precisa estar logado para continuar"
          : "Erro Interno no Servidor";
    } else {
      msgError = "Erro Interno no Servidor.";
    }

    yield put(EnterpriseActions.enterprisesFailure(msgError));
  }
}
