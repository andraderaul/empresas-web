import { call, put } from "redux-saga/effects";

import AutenticationService from "../../Services/AutenticationServices";
import LocalStorageService from "../../Services/Utils/LocalStorageServices";

import { Creators as SignInActions } from "../Ducks/SignIn";

export function* signIn(action) {
  const autenticationService = AutenticationService.create();
  try {
    const body = {
      email: action.payload.email,
      password: action.payload.password,
    };
    const response = yield call(autenticationService.signInAsync, body);
    const { data, headers } = response;

    LocalStorageService.setToken(headers);

    yield put(SignInActions.signInSuccess(data));
  } catch (err) {
    console.log(err);
    let msgError = "";

    if (!err.response) {
      msgError = "Tempo limite excedido.";
    } else if (err.response.status === 401) {
      const {
        response: {
          data: { errors },
        },
      } = err;

      msgError =
        errors.length > 0
          ? "Credenciais informadas são inválidas, tente novamente."
          : "Erro Interno no Servidor";
    } else {
      msgError = "Erro Interno no Servidor.";
    }

    yield put(SignInActions.signInFailure(msgError));
  }
}
