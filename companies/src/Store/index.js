import {createStore, compose, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';

import reducers from './Ducks';
import sagas from './Sagas';

import Config from '../Config/DebugConfig';

const sagaMonitor = Config.useReactotron
  ? console.tron.createSagaMonitor()
  : null;

const sagaMiddleware = createSagaMiddleware({sagaMonitor});
const middlewares = [sagaMiddleware];

const composer = Config.useReactotron
  ? compose(applyMiddleware(...middlewares), console.tron.createEnhancer())
  : applyMiddleware(...middlewares);

const store = createStore(reducers, composer);

sagaMiddleware.run(sagas);

export default store;
