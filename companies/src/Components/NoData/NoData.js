import React from "react";
import PropTypes from "prop-types";

import "./NoData.styles.css";

const NoData = ({ message }) => {
  return (
    <div className="no-data-component">
      <span className="text-style-3 ">{message}</span>
    </div>
  );
};

NoData.propTypes = {
  message: PropTypes.string.isRequired,
};

export default NoData;
