import React from "react";
import { AlertCircle } from "react-feather";
import PropTypes from "prop-types";

import "./Error.styles.css";
import * as Colors from "../../Helpers/Styles/Colors";

const Error = ({ message }) => {
  return (
    <div className="error-component">
      <span>{message}</span>
      <AlertCircle size="40" color={Colors.red} />
    </div>
  );
};

Error.propTypes = {
  message: PropTypes.string.isRequired,
};

export default Error;
