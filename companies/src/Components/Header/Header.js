import React, { useState } from "react";
import PropTypes from "prop-types";
import { Search, ArrowLeft } from "react-feather";

import "./Header.styles.css";
import * as Colors from "../../Helpers/Styles/Colors";
import ResponsiveImage from "../ResponsiveImage/ResponsiveImage";
import InputSearch from "../InputSearch/InputSearch";
import Logo from "../../Helpers/Assets/Images/logo-nav/logo-nav.png";

const Header = ({ isDetail, title, ...rest }) => {
  const [search, setSearch] = useState(false);

  const toggleSearch = () => setSearch((search) => !search);

  const handleBack = () => rest.history.goBack();

  return (
    <nav className={`${isDetail && "nav-detail"}`}>
      {isDetail ? (
        <>
          <ArrowLeft
            className="arrow-icon"
            size="40"
            color={Colors.white}
            onClick={handleBack}
          />
          {title && <span className="text-style-5">{title}</span>}
        </>
      ) : !search ? (
        <>
          <ResponsiveImage src={Logo} alt="Logo da Ioasys" />
          <Search
            className="search-icon"
            size="40"
            color={Colors.white}
            onClick={toggleSearch}
          />
        </>
      ) : (
        <InputSearch toggleSearch={toggleSearch} />
      )}
    </nav>
  );
};

Header.propTypes = {
  isDetail: PropTypes.bool,
  title: PropTypes.string,
};

export default Header;
