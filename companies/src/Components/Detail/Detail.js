import React from "react";
import PropTypes from "prop-types";

import { initialNameEnterprise } from "../../Helpers/Utils/Util";
import "./Detail.styles.css";

const Detail = ({ enterpriseName, description }) => {
  return (
    <div className="detail">
      <div className="image">
        <span className="text-style-3">
          {initialNameEnterprise(enterpriseName)}
        </span>
      </div>
      <span>{description}</span>
    </div>
  );
};

Detail.propTypes = {
  enterpriseName: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default Detail;
