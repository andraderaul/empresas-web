import React, { useState } from "react";
import PropTypes from "prop-types";
import { Eye, EyeOff, AlertCircle } from "react-feather";

import * as Colors from "../../Helpers/Styles/Colors";
import "./PrimaryInput.styles.css";

const PrimaryInput = ({
  placeholder,
  className = "",
  type,
  value,
  onChange,
  onBlur,
  renderIcon,
  error,
  invalid,
  secureTextEntry,
}) => {
  const [typeState, setTypeState] = useState(type);
  const handlOnChangeType = (newType) => setTypeState(newType);

  const Icon = () => renderIcon();

  return (
    <div className="primary-input">
      <div className="content">
        {secureTextEntry &&
          (typeState === "password" ? (
            <Eye
              className="icon-right"
              color={Colors.pink}
              size="18"
              onClick={() => handlOnChangeType("text")}
            />
          ) : (
            <EyeOff
              className="icon-right"
              color={Colors.pink}
              size="18"
              onClick={() => handlOnChangeType("password")}
            />
          ))}

        {invalid && (
          <>
            <div className="icon-right invalid-credentials" />
            <AlertCircle
              className="icon-right"
              color={Colors.white}
              size="18"
            />
          </>
        )}
        <Icon />
        <input
          placeholder={placeholder}
          className={`${className} ${!!error && "border-error"} ${
            invalid && "border-error"
          }`}
          type={typeState}
          value={value}
          onChange={onChange}
          onBlur={onBlur}
        />
      </div>
      {!!error && <span className="error-message">{error.message}</span>}
    </div>
  );
};

PrimaryInput.propTypes = {
  placeholder: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  renderIcon: PropTypes.func,
  secureTextEntry: PropTypes.bool,
  invalid: PropTypes.bool,
};

export default PrimaryInput;
