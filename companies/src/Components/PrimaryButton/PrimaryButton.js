import React from "react";
import PropTypes from "prop-types";

import "./PrimaryButton.styles.css";

const PrimaryButton = ({ title, onClick, disabled, className }) => {
  return (
    <button
      className={`${className} primary-button ${
        disabled ? "disabled" : "active"
      }`}
      onClick={onClick}
      disabled={disabled}
    >
      <span className="title text-style-5">{title}</span>
    </button>
  );
};

PrimaryButton.propTypes = {
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  className: PropTypes.string,
};

export default PrimaryButton;
