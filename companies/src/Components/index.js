import PrimaryInput from "./PrimaryInput/PrimaryInput";
import PrimaryButton from "./PrimaryButton/PrimaryButton";
import ResponsiveImage from "./ResponsiveImage/ResponsiveImage";
import Header from "./Header/Header";
import Card from "./Card/Card";
import Detail from "./Detail/Detail";
import Loading from "./Loading/Loading";
import Error from "./Error/Error";
import NoData from "./NoData/NoData";

export {
  PrimaryInput,
  PrimaryButton,
  ResponsiveImage,
  Header,
  Card,
  Detail,
  Loading,
  Error,
  NoData,
};
