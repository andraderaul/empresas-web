import React from "react";
import PropTypes from "prop-types";

import "./ResponsiveImage.js";

const ResponsiveImage = ({ src, alt, className }) => {
  return (
    <div className={`${className} image`}>
      <img alt={alt} src={src} />
    </div>
  );
};

ResponsiveImage.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default ResponsiveImage;
