import React from "react";
import PropTypes from "prop-types";

import { initialNameEnterprise } from "../../Helpers/Utils/Util";
import "./Card.styles.css";

const Card = ({ enterpriseName, country, enterpriseType, onClick }) => {
  return (
    <div className="card" onClick={onClick}>
      <div className="image">
        <span className="text-style-3">
          {initialNameEnterprise(enterpriseName)}
        </span>
      </div>
      <div className="content">
        <span className="text-style-6 enterprise-name">{enterpriseName}</span>
        <span className="text-style-7 enterprise-type-name">
          {enterpriseType}
        </span>
        <span className="text-style-8 enterprise-country">{country}</span>
      </div>
    </div>
  );
};

Card.propTypes = {
  enterpriseName: PropTypes.string.isRequired,
  enterpriseType: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Card;
