import React, { useState } from "react";
import PropTypes from "prop-types";
import { Search, X } from "react-feather";

/* redux */
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as EnterpriseActions } from "../../Store/Ducks/Enterprise";

import "./InputSearch.styles.css";
import * as Colors from "../../Helpers/Styles/Colors";

const InputSearch = ({ toggleSearch, enterprisesRequest, enterprises }) => {
  const { enterpriseTypes } = enterprises;
  const [querySearch, setQuerySearch] = useState("");

  const handleOnChange = (event) => {
    setQuerySearch(event.target.value);
  };

  const findEnterpriseType = () =>
    enterpriseTypes.find(
      (currentEnterprise) =>
        currentEnterprise.enterprise_type_name.toLowerCase() ===
        querySearch.toLocaleLowerCase()
    );

  const handleSearch = () => {
    const enterpriseType = findEnterpriseType();

    enterprisesRequest({
      type: !!enterpriseType ? enterpriseType.id : "",
      name: !!enterpriseType ? "" : querySearch.trim(),
    });
  };

  const handleOnSubmit = (event) => {
    event.preventDefault();
    handleSearch();
  };

  return (
    <div className="input-search">
      <Search
        className="icon-left "
        size="40"
        color={Colors.white}
        onClick={handleSearch}
      />
      <form onSubmit={handleOnSubmit}>
        <input
          className="text-danger"
          placeholder="Pesquisar"
          onChange={handleOnChange}
          value={querySearch}
        />
      </form>
      <X
        className="icon-right"
        size="40"
        color={Colors.white}
        onClick={toggleSearch}
      />
    </div>
  );
};

InputSearch.propTypes = {
  toggleSearch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  enterprises: state.enterprises,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(EnterpriseActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InputSearch);
