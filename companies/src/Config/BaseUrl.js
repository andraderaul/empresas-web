const BaseURL = {
  autentication: "https://empresas.ioasys.com.br/api/v1/users/auth",
  enterprises: "https://empresas.ioasys.com.br/api/v1/enterprises",
};

export default BaseURL;
