import { runSaga } from "redux-saga";
import MockAdapter from "axios-mock-adapter";
import { Creators as SignInActions } from "../../Store/Ducks/SignIn";
import { signIn } from "../../Store/Sagas/SignIn";
import AutenticationService from "../../Services/AutenticationServices";

//const apiMock = new MockAdapter(AutenticationService.create());

const response = {
  investor: {
    id: 1,
    investor_name: "Teste Apple",
    email: "testeapple@ioasys.com.br",
    city: "BH",
    country: "Brasil",
    balance: 350000.0,
    photo: "/uploads/investor/photo/1/cropped4991818370070749122.jpg",
    portfolio: {
      enterprises_number: 0,
      enterprises: [],
    },
    portfolio_value: 350000.0,
    first_access: false,
    super_angel: false,
  },
  enterprise: null,
  success: true,
};

describe("Sign in Sagas", () => {
  it("should be able to fetch sign in", async () => {
    const dispatched = [];

    //apiMock.onPost("/sign_in").reply(200, ["REre", "rere"]);

    // await runSaga(
    //   {
    //     dispatch: (action) => {
    //       dispatched.push(action);
    //     },
    //   },
    //   signIn
    // ).toPromise();

    // expect(dispatched).toContainEqual(SignInActions.signInSuccess(response));
  });
});
