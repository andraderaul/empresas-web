import signInReducer, {
  Creators as SignInActions,
} from "../../Store/Ducks/SignIn";

describe("Sign in Reducer", () => {
  it("should be able to sign in request", () => {
    const state = signInReducer(
      {
        isLoading: false,
      },
      SignInActions.signInRequest({
        email: "email@example.com",
        password: "password",
      })
    );

    expect(state.isLoading).toBe(true);
  });

  it("should be able to sign in success", () => {
    const state = signInReducer(
      {
        data: {
          investor: { id: "" },
        },
      },
      SignInActions.signInSuccess({
        investor: { id: 1 },
      })
    );

    expect(state.data.investor.id).toBe(1);
  });

  it("should be able to sign in failure", () => {
    const state = signInReducer(
      {
        error: null,
      },
      SignInActions.signInFailure(
        "Credenciais informadas são inválidas, tente novamente."
      )
    );

    expect(state.error).toBe(
      "Credenciais informadas são inválidas, tente novamente."
    );
  });
});
