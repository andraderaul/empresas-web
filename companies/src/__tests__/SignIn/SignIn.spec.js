import React from "react";
import { mount } from "enzyme";
import { Provider } from "react-redux";
import createStore from "redux-mock-store";

import { SignInPage } from "../../Pages";

const mockSignIn = createStore();
const signInStore = mockSignIn({
  signIn: {
    isLoading: false,
    data: {
      investor: { id: "" },
    },
    error: null,
  },
});

describe("SignIn.spec", () => {
  const wrapper = mount(
    <Provider store={signInStore}>
      <SignInPage />
    </Provider>
  );

  it("should render BEM-VINDO AO EMPRESAS", () => {
    expect(
      wrapper.contains(
        <h1 className="text-style-10 welcome-text">Bem-vindo ao empresas</h1>
      )
    ).toBe(true);
  });

  it("should render Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.", () => {
    expect(
      wrapper.contains(
        <h2 className="text-style description-text">
          Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
        </h2>
      )
    ).toBe(true);
  });

  it("should be render invalid email", () => {
    wrapper.find('input[type="email"]').simulate("change", {
      target: { value: "theandraderaulgmail.com" },
    });

    expect(
      wrapper
        .find('span[className="error-message"]')
        .contains(<span className="error-message">E-mail inválido</span>)
    );
  });
});
