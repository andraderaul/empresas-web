import SignInPage from "./SignIn/SignInPage";
import HomePage from "./Home/HomePage";
import DetailPage from "./Detail/DetailPage";

export { SignInPage, HomePage, DetailPage };
