import React, { useEffect } from "react";

/* redux */
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as EnterpriseActions } from "../../Store/Ducks/Enterprise";
import { Creators as DetailActions } from "../../Store/Ducks/Detail";

import "./HomePage.styles.css";
import { Header, Card, Loading, Error, NoData } from "../../Components";

const HomePage = ({
  enterprises,
  enterprisesRequest,
  detailRequest,
  history,
}) => {
  const {
    data: { enterprises: enterpriseList },
    isLoading,
    error,
  } = enterprises;

  useEffect(() => {
    enterprisesRequest({ type: "", name: "" });
    return () => {};
  }, [enterprisesRequest]);

  const handleEnterprise = (id) => {
    detailRequest({ id });
    history.push("/detail");
  };

  return (
    <div className="home">
      <Header />
      {isLoading ? (
        <Loading />
      ) : !!error ? (
        <Error message={error} />
      ) : enterpriseList.length === 0 ? (
        <NoData message="Nenhuma empresa foi encontrada para a busca realizada." />
      ) : (
        enterpriseList.map(
          ({
            id,
            enterprise_name,
            country,
            enterprise_type: { enterprise_type_name },
          }) => (
            <Card
              key={id}
              enterpriseName={enterprise_name}
              country={country}
              enterpriseType={enterprise_type_name}
              onClick={() => handleEnterprise(id)}
            />
          )
        )
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  enterprises: state.enterprises,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...EnterpriseActions, ...DetailActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
