import React from "react";

/* redux */
import { connect } from "react-redux";

import "./DetailPage.styles.css";
import { Detail, Header, Loading, Error } from "../../Components";

const DetailPage = ({ detail, ...rest }) => {
  const {
    data: { enterprise },
    isLoading,
    error,
  } = detail;

  return (
    <div className="detail-page">
      <Header isDetail title={enterprise?.enterprise_name} {...rest} />
      {isLoading ? (
        <Loading />
      ) : !!error ? (
        <Error message={error} />
      ) : (
        <Detail
          enterpriseName={enterprise.enterprise_name}
          description={enterprise.description}
        />
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  detail: state.detail,
});

export default connect(mapStateToProps)(DetailPage);
