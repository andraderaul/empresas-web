import React, { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import { Mail, Unlock } from "react-feather";

/* redux */
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as SignInActions } from "../../Store/Ducks/SignIn";

import { PrimaryInput, PrimaryButton, Loading } from "../../Components";
import * as Colors from "../../Helpers/Styles/Colors";
import Logo from "../../Helpers/Assets/Images/logo-home/logo-home.png";
import "./SignInPage.styles.css";

const SignInPage = ({ signInRequest, signIn }) => {
  const { isLoading, error, data } = signIn;

  const [errorState, setErrorState] = useState(error);
  useEffect(() => {
    setErrorState(error);
    return () => {};
  }, [error]);

  const { control, handleSubmit, errors, formState } = useForm({
    mode: "onChange",
  });

  const handleSignIn = (data) => {
    signInRequest(data);
  };

  const isDisabled = () => !formState.isValid;

  return (
    <div className="sign-in">
      {isLoading && <Loading />}
      <div className="image">
        <img alt="Logo da Ioasys" src={Logo} />
      </div>
      <div className="welcome">
        <h1 className="text-style-10 welcome-text">Bem-vindo ao empresas</h1>
      </div>
      <div className="description">
        <h2 className="text-style description-text">
          Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
        </h2>
      </div>
      <div className="form-sign-in">
        <Controller
          control={control}
          render={({ onChange, onBlur, value }) => {
            return (
              <PrimaryInput
                type="email"
                placeholder="E-mail"
                error={errors?.email}
                invalid={!!errorState}
                onChange={(...event) => {
                  setErrorState(null);
                  return onChange(...event);
                }}
                value={value}
                onBlur={onBlur}
                renderIcon={() => (
                  <Mail className="icon-left" color={Colors.pink} size="18" />
                )}
              />
            );
          }}
          name="email"
          rules={{
            required: "O e-mail é obrigatório",
            pattern: {
              value: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
              message: "E-mail inválido",
            },
          }}
          defaultValue=""
        />
        <Controller
          control={control}
          render={({ onChange, onBlur, value }) => (
            <PrimaryInput
              type="password"
              placeholder="Senha"
              secureTextEntry
              error={errors?.password}
              invalid={!!errorState}
              value={value}
              onBlur={onBlur}
              onChange={(...event) => {
                setErrorState(null);
                return onChange(...event);
              }}
              renderIcon={() => (
                <Unlock className="icon-left" color={Colors.pink} size="18" />
              )}
            />
          )}
          name="password"
          rules={{ required: "A senha é obrigatória" }}
          defaultValue=""
        />
        {!!errorState && (
          <div className="content-msg-error">
            <span className="text-style msg-error">{errorState}</span>
          </div>
        )}

        <PrimaryButton
          className={!!errorState ? "" : "button"}
          onClick={handleSubmit(handleSignIn)}
          title="Entrar"
          disabled={isDisabled()}
        />
        {!!data.investor.id && <Redirect to="/home" />}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  signIn: state.signIn,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(SignInActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SignInPage);
