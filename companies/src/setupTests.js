import "@testing-library/jest-dom/extend-expect";
import "@testing-library/jest-dom";
import "mutationobserver-shim";

import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";

global.MutationObserver = window.MutationObserver;
Enzyme.configure({ adapter: new Adapter() });
